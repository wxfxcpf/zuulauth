package com.tianyalei.core.zuulauth.config;

import com.tianyalei.core.zuulauth.cache.AuthCache;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.StringRedisTemplate;

import javax.annotation.Resource;

/**
 * @author wuweifeng wrote on 2019-08-14.
 */
@Configuration
@ConditionalOnMissingBean({AuthConfigure.class, ClientAuthCacheConfigure.class}) //不是zuul工程时，才启用该配置
public class ClientAuthCacheConfigure {
    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Bean
    @ConditionalOnMissingBean
    AuthCache authCache() {
        return new AuthCache(stringRedisTemplate);
    }
}
