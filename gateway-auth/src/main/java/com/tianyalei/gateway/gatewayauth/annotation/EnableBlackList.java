package com.tianyalei.gateway.gatewayauth.annotation;

import com.tianyalei.gateway.gatewayauth.config.BlackListConfigure;
import org.springframework.context.annotation.Import;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author liyunfeng 2020-1-14
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import(BlackListConfigure.class)
public @interface EnableBlackList {

}